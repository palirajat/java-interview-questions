
class X
{
    static int x = 3131;
     
    static class Y
    {
        static int y = x++;
         
        static class Z
        {
        	int a = 10;
            static int z = y++;
        }
    }
}

class A{
	{
		System.out.print("AAAAAAA \n");
	}
	
	A(){
		System.out.print("AAA Cons \n");
	}
	
	{
		System.out.print("XXXXX \n");
	}
}

class D extends A{
	{
		System.out.print("DDDDDDD \n");
	}
	
	D (){
		System.out.print("DDDD COns \n");
	}
	

	{
		System.out.print("YYYYY \n");
	}
}

public class ClassForNested
{
    public static void main(String[] args)
    {
        System.out.println(X.x);
         
        System.out.println(X.Y.y);
         
        System.out.println(X.Y.Z.z);
        new D();
    }
}