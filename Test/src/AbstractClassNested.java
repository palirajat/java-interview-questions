
abstract class A1
{
	A1(){
		 System.out.println(0);
	}
    {
        System.out.println(1);
    }
     
    static
    {
        System.out.println(2);
    }
    
    public void method1() {
    	System.out.println("method1");
    }
    abstract void method2() ;
}

public class AbstractClassNested {

	public static void main(String[] args)
    {
        A1 a = new A1() { 
        	void method2() {
        		System.out.println("method2");	
        	}
        	
        };
        a.method1();
        a.method2();
    }
}
