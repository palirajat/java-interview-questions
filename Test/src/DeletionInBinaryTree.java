

public class DeletionInBinaryTree {

	static int max_level = 0;
	Node root =null;
	public Node  insertInBST(Node root , int key) {
		if(root==null) {
			root = new Node(key);
			return root;
		}
		if(key<root.value) {
			root.left = insertInBST(root.left,key);
		} else if (key>root.value) {
			root.right =insertInBST(root.right, key);
		}
		
		return root;
	}
	
	public static void main(String[] args) {
		DeletionInBinaryTree tree = new DeletionInBinaryTree();
		tree.root = tree.insertInBST(tree.root,50); 
		tree.root = tree.insertInBST(tree.root,30); 
		tree.root =  tree.insertInBST(tree.root,20); 
		tree.root = tree.insertInBST(tree.root,40); 
		tree.root = tree.insertInBST(tree.root,70); 
		tree.root = tree.insertInBST(tree.root,60); 
		tree.root = tree.insertInBST(tree.root,80);
		//tree.deleteNode(tree.root, 50);
		//tree.deleteNode(tree.root, 30);
		tree.leftViewOfTree(tree.root, 1);
		System.out.println();
		max_level = 0;
		tree.rightViewOfTree(tree.root, 1);
		
		System.out.println("LCA of 20 & 40 is "+tree.findLCA(tree.root, 20, 40).value);
	}
	
	// delete a node from a tree
	public Node deleteNode(Node root , int key) {
		if(root ==null) {
			return root;
		}
		if(key <root.value) {
			root.left = deleteNode(root.left, key);
		}else if(key>root.value) {
			root.right = deleteNode(root.right, key);
		} else {
			if(root.left == null ) {
				return root.right;
			} else if(root.right ==null) {
				return root.left;
			}
			root.value = minValueRight(root.right);
			root.right = deleteNode(root.right, root.value);
		}
		
		return root;
	}
	
	public int minValueRight(Node root) {
		while(root.left!=null) {
			root = root.left;
		}
		return root.value;
	}
	
	
	//left view of a tree
	public void leftViewOfTree (Node root,int level) {
		if(root == null) {
			return;
		}
		if(max_level <level) {
			System.out.print(root.value+",");
			max_level = level;
		}
		leftViewOfTree(root.left, level+1);
		leftViewOfTree(root.right, level+1);
		
	}
	
	//right view of a tree
	public void rightViewOfTree (Node root,int level) {
		if(root == null) {
			return;
		}
		if(max_level <level) {
			System.out.print(root.value+",");
			max_level = level;
		}
		rightViewOfTree(root.right, level+1);
		rightViewOfTree(root.left, level+1);
		
	}
	
	
  public Node findLCA(Node node, int n1, int n2) {
	  if(node == null) {
		  return null;
	  }
	  
	  if(node.value == n1 || node .value ==n2) {
		  return node;
	  }
	  
	  Node left_lca = findLCA(node.left,n1,n2);
	  Node right_lca = findLCA(node.right,n1,n2);
	  
	  if (left_lca!=null && right_lca!=null) 
          return node; 

	  return (left_lca != null) ? left_lca : right_lca; 
  }
	
}
