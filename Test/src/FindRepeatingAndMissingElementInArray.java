
public class FindRepeatingAndMissingElementInArray {
	
	public static void main(String[] args) {
		int arr[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,1,15};
		int temp[] = new int[arr.length];
		for(int i =0;i<arr.length;i++) {
			if(temp[arr[i]-1] == 0) {
				temp[arr[i]-1] = 1;
			} else {
				System.out.println("Duplicate element is : "+arr[i]);
			}
		}
		for(int i =0;i<temp.length;i++) {
			if(temp[i] == 0) {
				System.out.println("Missing element is : "+(i+1));
			} 
		}
		
	}

}
