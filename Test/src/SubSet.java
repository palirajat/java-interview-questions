
public class SubSet {

	
	public static void main(String[] args) {
		int arr[] = {2,5,8,11,6,9,13,1,17,33};
		int sum = 22;
		for(int i= 0;i<arr.length;i++) {
			for(int j=i;j<arr.length;j++) {
				if(arr[i]> arr[j]) {
					int temp = arr[i];
					arr[i]= arr[j];
					arr[j]= temp;
				}
			}
		}
		boolean flag = false;
		for(int i= 0;i<arr.length;i++) {
			for(int j=i;j<arr.length;j++) {
				if(arr[i] + arr[j]== sum) {
					System.out.println("Pair are "+arr[i]+" , "+arr[j]+" = "+sum);
					flag = true;
					break;
				} else if(arr[i] + arr[j]>sum) {
					break;
				}
			}
			if(flag)break;
		}
		if(!flag) {
			System.out.println("No pair found");
		}
	}
}
