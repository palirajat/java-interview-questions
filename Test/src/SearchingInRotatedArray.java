import java.util.Arrays;

public class SearchingInRotatedArray {
	
	
	public static void main(String[] args) {
		int arr[]= {11,15,18,22,32,38,44,1,3,5,7,9};
		int searchValue = 17;
		int pivotIndex = SearchingInRotatedArray.getPivotIndex(arr);
		int index1 = SearchingInRotatedArray.binarySearch(Arrays.copyOfRange(arr, 0, pivotIndex+1),searchValue);
		int index2 = SearchingInRotatedArray.binarySearch(Arrays.copyOfRange(arr,pivotIndex+1,arr.length-1),searchValue);
		
		if(index1==-1 && index2 ==-1) {
			System.out.print("Element is not present.");
		} else {
			if(index1>-1) {
				System.out.print("Element is present at index : "+index1);
			} else if(index2>-1){
				System.out.println("Element is present at index : "+(pivotIndex+index2+1));
			} else {
			System.out.print("Element is not present.");
			}
		}
	}
	
	public static int getPivotIndex(int arr[]) {
		for(int i=0;i<arr.length-1 ; i++) {
			if(arr[i]>arr[i+1]) {
				return i;
			}
		}
		return -1;
	}
	
	public static int binarySearch(int arr[],int data) {
		int left = 0;
		int right = arr.length-1;
		int mid = arr.length/2;
		int temp ;
		while(left<=right) {
			temp = (left +right)/2;
			if(data<arr[mid]) {
				right = mid-1;
				
			} else if(data>arr[mid]) {
				left = mid+1;
			} else {
				return mid;
			}
			mid = temp;
			
		}
		return -1;
	}
	
	public static void  printArray(int arr[]) {
		for(int value : arr) {
			System.out.print("\t"+value);
		}
		System.out.println();
	}
	
}
