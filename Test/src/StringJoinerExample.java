import java.util.StringJoiner;

public class StringJoinerExample {

	public static void main(String[] args) {
		String query  = "INSERT INTO table_name";
		StringJoiner strColNameJoiner = new StringJoiner(","," (",")");
		strColNameJoiner.add("col1");
		strColNameJoiner.add("col2");
		strColNameJoiner.add("col3");
		strColNameJoiner.add("col4");
		strColNameJoiner.add("col5");
		strColNameJoiner.add("col6");
		strColNameJoiner.add("col7");
		strColNameJoiner.add("col8");
		strColNameJoiner.add("col9");
		strColNameJoiner.add("col10");
		
		StringJoiner strColValueJoiner = new StringJoiner(","," VALUES(",")");
		strColValueJoiner.add("val1");
		strColValueJoiner.add("val2");
		strColValueJoiner.add("val3");
		strColValueJoiner.add("val4");
		strColValueJoiner.add("val5");
		strColValueJoiner.add("val6");
		strColValueJoiner.add("val7");
		strColValueJoiner.add("val8");
		strColValueJoiner.add("val9");
		strColValueJoiner.add("val10");
		
		query = query +strColNameJoiner.toString()+strColValueJoiner.toString();
		
		System.out.println(query);
		
	}
}
