import java.util.LinkedList;
import java.util.Queue;

class Node{
	int value;
	Node left;
	Node right;
	Node(int data){
		this.value = data;
	}
}
public class BinaryTreeInsertion {
	Node root =null;
	public boolean add(Node node ) {
		if(root == null) {
			root = node;
			return true;
		}
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while(!queue.isEmpty()) {
		Node temp = queue.poll();
		if(temp.left==null) {
			temp.left = node;
			break;
		} else {
			queue.add(temp.left);
		}
		
		if(temp.right==null) {
			temp.right = node;
			break;
		} else {
			queue.add(temp.right);
		}
		}
		
		
		return true;
	}
	
	public static void main(String[] args) {
		BinaryTreeInsertion obj = new BinaryTreeInsertion();
		
			obj.add(new Node(4));
			obj.add(new Node(2));
			obj.add(new Node(5));
			obj.add(new Node(3));
			obj.add(new Node(1));
			obj.preOrderTraversal(obj.root);
			System.out.println();
			System.out.println("is Bst : "+obj.validateBST(obj.root));
			
		
	}
	
	public void preOrderTraversal(Node node) {
		if(node==null) {
			return;
		}
		preOrderTraversal(node.left);
		System.out.print("\t"+node.value);
		preOrderTraversal(node.right);
		
	}
	
	public boolean validateBST(Node node) {
		if(node==null || node.left == null || node.right==null) {
			return true;
		} if((node.value >node.left.value && node.value<node.right.value)) {
			return validateBST(node.left) && validateBST(node.right);
		}
		return false;
		
	}
	
}


