
import java.io.BufferedReader;
import java.io.InputStreamReader;

//import for Scanner and other utility classes
import java.util.*;


// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail

class TestClass {
    public static void main(String args[] ) throws Exception {
  

      
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int totalNumbersDisplay = Integer.parseInt(br.readLine());              // Reading input from STDIN
        String secondLine = br.readLine();
        String number[] = secondLine.split(" ");
        
        Map<String,Integer> mapValues = new HashMap<>();
        for(int i=0;i<number.length;i++){
            if(mapValues.containsKey(number[i])){
                mapValues.put(number[i],mapValues.get(number[i])+1);
            } else {
                 mapValues.put(number[i],1);
            }
            
        }
        
                int totalNumbersToCheck = Integer.parseInt(br.readLine());              // 
                String searchKey[] = new String[totalNumbersToCheck];
                for(int k=0;k<totalNumbersToCheck;k++){
                   searchKey[k]=br.readLine();  
                }
        for(int j =0 ;j<searchKey.length;j++){
            if(mapValues.containsKey(searchKey[j])){
                System.out.println(mapValues.get(searchKey[j]));
            } else {
            	System.out.println("NOT PRESENT");
            }
        }

       
        // Write your code here

    }
}
