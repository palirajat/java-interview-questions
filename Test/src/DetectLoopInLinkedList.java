
public class DetectLoopInLinkedList {

	public static void main(String[] args) {
		Linkedlist list = new Linkedlist();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.startNode.nextNode.nextNode.nextNode.nextNode = list.startNode.nextNode;
		boolean flag = Linkedlist.detectLoop(list.startNode);
		if(flag) {
			 Linkedlist.removeLoop(list.startNode);
		}
		LinkedListNode temp = list.startNode;
		while(temp!=null) {
			
			System.out.print("\t"+temp.value);
			temp = temp.nextNode;
		}
		
	}
}
