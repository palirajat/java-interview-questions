import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class StreamInJava8 {

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(3,4,1,7,9,10,6,8,12,33,22);
		//map method used to map the existing collection into another collection without 
		//effecting existing collection
		List<Integer> listMap = list.stream().map(value->value*value).collect(Collectors.toList());
		// remove duplicate elements
		Set<Integer>  setMap = list.stream().map(value->value*value).collect(Collectors.toSet());
		System.out.println(list);
		System.out.println(listMap);
		System.out.println(setMap);
		
		//filter method use to apply some condition on existing collection and map it with other new collection
		List<Integer> listFilter = list.stream().filter(value-> value>10).collect(Collectors.toList());
		System.out.println(listFilter);
		
		// sorted method is used to sort the existing collection and map it with other new collection 
		List<Integer> listSort = list.stream().sorted().collect(Collectors.toList());
		System.out.println(listSort);
		Set<Integer> setSort = list.stream().sorted().collect(Collectors.toSet());
		System.out.println(setSort);
		
		//collect , forEach && reduce methods are terminal method
		// reduce method is used to reduce the elements of a stream to a single value;
		
		int sum = list.stream().filter(value->value<=10).reduce(0,(result,value)->result+value);
		int multi = list.stream().filter(value->value<=10).reduce(1,(result,value)->result*value);
		System.out.println("sum is : "+sum);
		System.out.println("multi is : "+multi);
		List<Integer> listLimit= listSort.stream().limit(3).collect(Collectors.toList());
		System.out.println(listLimit);
		System.out.println("------------");
		System.out.println(list.stream().map(value->value*value).filter(value->value%2==0).count());
		System.out.println("------------");
		list.stream().map(value->value*value).filter(value->value%2==0).forEach(System.out::println);
	}
	
}
