interface Drawable{  
    public void draw();  
} 

interface addition{
	public int addition(int value1 , int value2);
}
interface subtraction{
	public int subtraction(int value1 , int value2);
}
interface multiplication{
	public int multiplication(int value1 , int value2);
}
interface division{
	public int division(int value1 , int value2);
}
public class LambdaExpressionExample {  
    public static void main(String[] args) {  
        int width=10;  
  
        Drawable d=new Drawable(){  
            public void draw(){
            	System.out.println("Drawing "+width);
            	}  
        };  
        d.draw(); 
        
        Drawable d2 =()->{
        	System.out.println("Drawing width by d2 "+width);
        }; 
        d2.draw();
        System.out.println(d==d2);
        
       addition addition = (num1,num2)->{
        	return num1+num2;
        };
        subtraction subtraction = (num1,num2)->{
        	return num1-num2;
        };
        multiplication multiplication = (num1,num2)->{
        	return num1*num2;
        };
        division division = (num1,num2)->{
        	return num1/num2;
        };
        System.out.println(addition.addition(10, 5));
        System.out.println(subtraction.subtraction(10, 5));
        System.out.println(multiplication.multiplication(10, 5));
        System.out.println(division.division(10, 5));
        
        
        
    }  
} 