
class Book {
	int id;
	String name;
	String author;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	double price;
	
	Book(int id , String name , String author , double price){
	this.id = id;
	this.name = name;
	this.author = author;
	this.price = price;
	}
}

class Printer {
	public void print(Book book) {
		{
			System.out.print(" id "+book.getId());
			System.out.print(" Name "+book.getName());
			System.out.print(" Author "+book.getAuthor());
			System.out.print(" Price "+book.getPrice());
			System.out.println();
		}
	}
}


class PrintBook extends Thread {
	Book book ;
	Printer printer;
	PrintBook(Book book,Printer printer){
		this.book = book;
		this.printer = printer;
	}
	public void run() {
		synchronized(printer) {
			printer.print(book);
		}
	}
}



public class SynchronizationPractise {
	
	public static void main(String[] args) {
		Printer printer =  new Printer();
		
		for(int i=1;i<=30;i++) {
			PrintBook printBook = new PrintBook( new Book(i,"Name : "+i,"Author : "+i,i*100),printer);
			printBook.start();
		}
	}
}
