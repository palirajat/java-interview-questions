
public class RemoveDuplicateFromString {
	public static void main(String[] args) {
		String str = "rajat";
		String result=RemoveDuplicateFromString.removeDuplicateCharacters(str);
		System.out.print(result);
	}
	
	public static String removeDuplicateCharacters(String str) {
		String result="";
		for(int i=0;i<str.length();i++) {
			if(str.lastIndexOf(str.charAt(i))==i) {
				result = result+str.charAt(i);
			}
		}
		return result;
	}

}
