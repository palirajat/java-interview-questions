import java.util.Stack;

public class SumOfTwoLinkedListElements {

	static int  number1 =0;
	static int  number2 =0;
	static int multiplier = 1;
	public static void main(String[] args) {
		Linkedlist list1 = new Linkedlist();
		for(int i=1;i<=4;i++) {
			list1.add(i);
		}
		Linkedlist list2 = new Linkedlist();
		for(int i=5;i<=8;i++) {
			list2.add(i);
		}
		Stack<Integer> list1Stack = new Stack<>();
		LinkedListNode tempList1 = list1.startNode;
		while(tempList1!=null) {
		list1Stack.push(tempList1.value);
		tempList1 = tempList1.nextNode;
		}
		
		Stack<Integer> list2Stack = new Stack<>();
		LinkedListNode tempList2 = list2.startNode;
		while(tempList2!=null) {
		list2Stack.push(tempList2.value);
		tempList2 = tempList2.nextNode;
		}
		
		while(list1Stack.size()!=0) {
			number1 = number1 +multiplier*list1Stack.pop();
			multiplier = multiplier*10;
		}
		
		multiplier = 1;
		
		while(list2Stack.size()!=0) {
			number2 = number2 +multiplier*list2Stack.pop();
			multiplier = multiplier*10;
		};
		
		int result = number1+number2;
		System.out.print("Sum of the two list number is : "+result);
		
	}
}
