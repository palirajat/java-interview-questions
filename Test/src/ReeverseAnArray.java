import java.util.Arrays;

public class ReeverseAnArray {

	public static void main(String[] args) {
		Integer arr[] = {1,2,3,4,5,6,7,8,9,10};
		int temp;
		for(int i=0;i<arr.length/2;i++) {
			temp = arr[i];
			arr[i]=arr[arr.length-1-i];
			arr[arr.length-1-i]  = temp;
		}
		Arrays.asList(arr).stream().peek(System.out::println).count();
	}
}
