import java.util.ArrayList;
import java.util.List;

public class MaximumProductOfRope {

	public static void main(String[] args) {
		int length  = 28;
		int tempLength = 28;
		int resultProduct = 1;
		int tempProduct =  1;
		List<Integer> resultRecords = new ArrayList<>();
		List<Integer> tempRecords = new ArrayList<>();
		for(int i=1;i<=length;i++) {
			tempRecords.clear();
			tempLength = length;
			tempProduct = 1;
			for(int j=i;j<=length;j++) {
			if(j<tempLength) {
			tempProduct = tempProduct *j;
			tempLength -=j;
			tempRecords.add(j);
			} else {
				tempProduct = tempProduct * tempLength;
				tempRecords.add(tempLength);
				break;
			}
			}
			if(tempProduct>resultProduct) {
				resultProduct = tempProduct;
				resultRecords = new ArrayList<Integer>(tempRecords);
			}
			
		}
		
		System.out.println("resultProduct "+resultProduct);
		System.out.println(resultRecords);
		
	}
}
