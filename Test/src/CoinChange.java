import java.util.ArrayList;
import java.util.List;

public class CoinChange {

	public static void main(String[] args) {
		int arr[] = {2, 5, 3, 6 , 4};
		int sum = 10;
		for(int i= 0;i<arr.length;i++) {
			for(int j=i;j<arr.length;j++) {
				if(arr[i]> arr[j]) {
					int temp = arr[i];
					arr[i]= arr[j];
					arr[j]= temp;
				}
			}
		}
		int temp = sum;
		List<Integer> list = new ArrayList<>();
		for(int i= 0;i<arr.length;i++) {
			list.clear();
			temp = sum;
			if(sum % arr[i] == 0) {
				while (temp!=0) {
					list.add(arr[i]);
					temp = temp -arr[i];
				}
				if(!list.isEmpty()) {
					System.out.println();
					for(int val : list) {
						System.out.print("\t"+val);
					}
				}
			}
		}
			
		
	}
	
}
