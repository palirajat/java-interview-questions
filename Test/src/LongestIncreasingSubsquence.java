import java.util.List;
import java.util.ArrayList;

public class LongestIncreasingSubsquence {

	public static void main(String[] args) {
		int arr[] = {10,4,9,22,11,8,27,31,20,38};
		List <Integer> resultList =  new ArrayList<>();
		List <Integer> tempList =  new ArrayList<>();
		for(int i= 0;i<arr.length;i++) {
			tempList.clear();
			tempList.add(arr[i]);
			for(int j= i+1;j<arr.length ;j++) {
				if(tempList.get(tempList.size()-1) < arr[j]) {
					tempList.add(arr[j]);
				}
			}
			if(resultList.size()<tempList.size()) {
				resultList = new ArrayList<>(tempList);
			}
		}
		System.out.println("Total elements "+resultList.size());
		System.out.println(resultList);
	}
}
