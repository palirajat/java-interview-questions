import java.util.HashSet;
import java.util.Set;

public class Linkedlist {

	LinkedListNode startNode =null;
	public void add(int data) {
		LinkedListNode temp = this.startNode;
		if(startNode == null) {
			startNode = new LinkedListNode(data);
		} else {
			while(temp.nextNode!=null) {
				temp = temp.nextNode;
			}
			temp.nextNode =  new LinkedListNode(data);
		}
		
	}
	
	public static boolean detectLoop(LinkedListNode startNode) {
		LinkedListNode slow = startNode;
		LinkedListNode fast = startNode;
		while(slow!=null && fast!=null && fast.nextNode!=null) {
			slow = slow.nextNode;
			fast = fast.nextNode.nextNode;
			if(slow==fast || slow.equals(fast)) {
				return true;
			}
		}
		
		return false;
	}
	
	public static void removeLoop(LinkedListNode startNode) {
		LinkedListNode temp = startNode;
		Set<LinkedListNode> set = new HashSet();
		LinkedListNode previous = startNode;
		while(temp!=null) {
			if(!set.add(temp)) {
				previous.nextNode = null;
				break;
			} else {
				previous = temp;
				temp = temp.nextNode;
			}
		}
	}
	
}

class LinkedListNode{
	int value;
	LinkedListNode nextNode;
	LinkedListNode(int data){
		this.value = data;
		this.nextNode = null;
	}
}
